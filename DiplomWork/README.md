
Prototyping-Library забезпечує легкий доступ до функцій виявлення особи IOS через NSNotification.

Visage конкретизується шляхом пропускання положення камери і оптимізації налаштувань для виконання, як це:
`` `
let visage = Visage (cameraPosition: Visage.CameraDevice.FaceTimeCamera, optimizeFor: Visage.DetectorAccuracy.HigherPerformance)
`` `

Для * cameraPosition * можливі варіанти:
* `.ISightCamera`, який використовує задню камеру в iPhone або IPad.
* `.FaceTimeCamera`, який використовує фронтальну камеру на iPhone або IPad.

Для * optimizeFor * можливі варіанти:
* `.HigherPerformance`, який встановлює CIDetectorAccuracy в CIDetectorAccuracyHigh.
* `.BatteryLife`, який встановлює CIDetectorAccuracy в CIDetectorAccuracyLow. (на старих пристроях)

Ряд подій:

* `VisageNoFaceDetectedNotification` спрацьовує коли особу не виявлено.
* `VisageFaceDetectedNotification` спрацьовує коли особу виявлено. 😐
* `VisageHasSmileNotification` спрацьовує, коли користувач посміхається. 😃
* `VisageHasNoSmileNotification`  коли користувач перестав посміхатися.
* `VisageBlinkingNotification` спрацьовує, коли користувач блимає. (2 закритих очі відразу)
* `VisageNotBlinkingNotification` коли користувач перестав блимати. (Принаймні одне око відкрите)
* `VisageWinkingNotification` спрацьовує, коли користувач підморгує. (1 око закрите) 😉
* `VisageNotWinkingNotification` спрацьовує, коли користувач не моргає чи моргає. (2 очі відкриті або закриті)
* `VisageLeftEyeClosedNotification`  коли користувач закрив ліве око.
* `VisageLeftEyeOpenNotification`  коли користувач відкрив ліве око.
* `VisageRightEyeClosedNotification`  коли користувач закрив праве око.
* `VisageRightEyeOpenNotification`  коли користувач відкрив праве око.

Щоб підписатись на ці події можна використовувати`NSNotificationCenter.defaultCenter (). AdObsever *`.

Visage надає купу властивостей:
* `FaceDetected: Bool` чи було особу виявлено ?.
* `FaceBounds :? CGRect` положення і розмір особи в пікселях на екрані.
* `FaceAngle: CGFloat` абсолютний кут особи в радіанах ?.
* `FaceAngleDifference: CGFloat` відносний кут особи (до попереднього) в радіанах.
* `LeftEyePosition: CGPoint` позиція лівого ока на екрані.
* `RightEyePosition: CGPoint` положення правого ока на екрані.
* `MouthPosition: CGPoint` положення рота на екрані ?.
* `HasSmile: чи є була посмішка виявлена ​​Bool` ?.
* `IsWinking: чи є було підморгнуто виявлений Bool` ?.
* `IsBlinking: чи мало моргнуто Bool` ?.
* `LeftEyeClosed: Bool` чи ліве око закрите чи ні ?.
* `RightEyeClosed: Bool` чи праве око закрите чи ні ?.




